package com.paul.hello;

public abstract class Car {
    // 车的名称
    private String name;

    // 车的租金
    private int price;

    // 载客人数
    private int passengerLoad;

    // 载货数量
    private double goodsLoad;

    public int getPrice() {
        return price;
    }

    public void setPrice(int rent) {
        this.price = rent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPassengerLoad() {
        return passengerLoad;
    }

    public void setPassengerLoad(int passengerLoad) {
        this.passengerLoad = passengerLoad;
    }

    public double getGoodsLoad() {
        return goodsLoad;
    }

    public void setGoodsLoad(double goodsLoad) {
        this.goodsLoad = goodsLoad;
    }
}
