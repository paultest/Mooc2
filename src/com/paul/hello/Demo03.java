/**
 * 静态变量：Java可以基于一个类创建多个该类的对象，每个对象都拥有自己的成员变量，互相独立，但是有些时候会希望该类所有的对象共享同一个成员变量，这个时候就需要静态变量
 *
 * Java中被static修饰的成员变量称之为静态成员或者类成员，它属于整个类所有，而不是某个对象所有，即被类的所有对象所共享。静态成员可以使用类名直接访问，也可以使用对象名进行访问，一般建议使用类名访问
 *
 * 使用static可以修饰变量、方法和方法块
 *
 * 注意：
 * 1. 静态成员属于整个类，当系统第一次使用该类时，就会为其分配内存空间直到该类被卸载才会进行资源回收
 * 2. 使用类名来访问静态变量的时候，前面可以不带类名，直接访问变量名即可，比如下面的System.out.println(Demo03.className);可以改成System.out.println(className);
 */
package com.paul.hello;

public class Demo03 {

    // static修饰的变量为静态变量，所有的类的对象共享该变量
    static String className = "JAVA开发一班";

    String className2 = "JAVA开发二班";

    public static void main(String[] args) {
        // 静态变量可以直接使用类名来访问，无需创建类的对象
        System.out.println(Demo03.className);

        // 创建类的对象
        Demo03 hello = new Demo03();

        // 静态变量也可以使用对象名来访问该变量
        System.out.println(hello.className);
        
        // 使用类名的形式来修改静态变量的值
        Demo03.className = "new name";

        System.out.println(hello.className);
        System.out.println(Demo03.className);

        // 使用对象名的形式修改静态变量的值
        hello.className = "new name2";

        System.out.println(hello.className);
        System.out.println(Demo03.className);

        hello.test();
    }

    public void test() {
        System.out.println("test方法里面了");

        // 静态变量可以直接使用类名来访问，无需创建类的对象
        System.out.println(Demo03.className);

        // 创建类的对象
        Demo03 hello = new Demo03();

        // 静态变量也可以使用对象名来访问该变量
        System.out.println(hello.className);

        System.out.println(className);
        System.out.println(className2);

        // 创建类的对象
        Demo03 hello1 = new Demo03();

        // 静态变量也可以使用对象名来访问该变量
        System.out.println(hello1.className2);
    }
}