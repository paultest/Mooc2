package com.paul.hello;

public class Demo13 {
    public static void main(String[] args) {
        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        dog1.age = 30;
        dog2.age = 30;
        if (dog1.equals(dog2)) {
            System.out.println("两个对象是一样的");
        } else {
            System.out.println("两个对象是不一样的");
        }
    }
}