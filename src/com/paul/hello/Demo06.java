/**
 * 内部类（ Inner Class ）就是定义在另外一个类里面的类。与之对应，包含内部类的类被称为外部类。
 *
 * 作用：
 * 1. 内部类提供了更好的封装，可以把内部类隐藏在外部类之内，不允许同一个包中的其他类访问该类
 * 2. 内部类的方法可以直接访问外部类的所有数据，包括私有的数据
 * 3. 内部类所实现的功能使用外部类同样可以实现，只是有时使用内部类更方便
 *
 * 分类：
 * 1. 成员内部类
 * 2. 静态内部类
 * 3. 方法内部类
 * 4. 匿名内部类
 */
package com.paul.hello;

//外部类Demo06
public class Demo06 {

    // 内部类Inner，类Inner在类Demo06的内部
    public class Inner {

        // 内部类的方法
        public void show() {
            System.out.println("welcome to imooc!");
        }
    }

    public static void main(String[] args) {

        // 创建外部类对象
        Demo06 hello = new Demo06();
        // 创建内部类对象
        Inner i = hello.new Inner();
        // 调用内部类对象的方法
        i.show();
    }
}