package com.paul.hello;

public class PickUpTruck extends Car {
    public PickUpTruck(String name, int price, int passengerLoad, double goodsLoad) {
        this.setName(name);
        this.setPrice(price);
        this.setPassengerLoad(passengerLoad);
        this.setGoodsLoad(goodsLoad);
    }

    @Override
    public String toString() {
        return this.getName() + "----" + this.getPrice() + "元/天---- 载客" + this.getPassengerLoad() + "人----载货" + this.getGoodsLoad() + "吨";
    }
}
