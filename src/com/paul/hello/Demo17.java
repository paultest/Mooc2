package com.paul.hello;

public class Demo17 {
    public static void main(String[] args) {
        HuaWei huawei = new HuaWei();
        huawei.call();
        huawei.message();
        huawei.playGame();

        // 如果实例化对象的时候，如果是以下这个方式的话，那么只能调用call和message方法，不能调用playGame方法，除非做转换((HuaWei) huawei1).playGame();
        Mobile huawei1 = new HuaWei();
        huawei1.call();
        huawei1.message();

        // 如果实例化对象的时候，如果是以下这个方式的话，那么只能调用playGame方法，不能调用call和message方法，除非做转换((HuaWei) huawei2).call();
        IPlayGame huawei2 = new HuaWei();
        huawei2.playGame();
    }
}
