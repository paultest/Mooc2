/**
 * 与静态变量一样，也可以使用static来修饰方法，称为静态方法或类方法
 *
 * 注意：
 * 1. 静态方法中可以直接调用同类中的静态成员，但不能直接调用非静态成员。
 * 2. 在普通成员方法中，则可以直接访问同类的非静态变量和静态变量
 * 3. 静态方法中不能直接调用非静态方法，需要通过对象来访问非静态方法
 */
package com.paul.hello;

public class Demo04 {

	/**
	 * 使用static关键字声明静态方法
	 */
	public static void print() {
		System.out.println("你好啊！");
	}
	
	public static void main(String[] args) {
		// 静态方法直接使用类名调用静态方法
		Demo04.print();
		
		// 静态方法也可以通过对象名调用
		Demo04 demo = new Demo04();
		demo.print();
	}

}