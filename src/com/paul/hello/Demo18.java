/**
 * 使用匿名内部类的方式实现接口
 * 接口在使用过程当中，还经常与匿名内部类配合使用，匿名内部类就是没有名字的内部类，多用于关注实现而不关注类的名称
 */
package com.paul.hello;

public class Demo18 {
    public static void main(String[] args) {
        IPlayGame ip1 = new IPlayGame() {
            @Override
            public void playGame() {
                System.out.println("使用匿名内部类的方式1实现接口");
            }
        };
        ip1.playGame();

        new IPlayGame() {
            @Override
            public void playGame() {
                System.out.println("使用匿名内部类的方式2实现接口");
            }
        }.playGame();
    }
}
