package com.paul.hello;

public class Demo14 {
    public static void main(String[] args) {
        // 父类的引用可以指向本类的对象
        Animal obj1 = new Animal();

        // 父类的引用可以指向子类的对象
        Animal obj2 = new Dog();
        Animal obj3 = new Cat();

        // 子类的引用不可以指向父类的对象
        // Dog obj3 = new Animal();

        obj1.eat();
        obj2.eat();
        obj3.eat();

        // 通过父类的引用创建的子类对象，是不允许调用子类独有的方法
        // obj2.method();
    }
}
