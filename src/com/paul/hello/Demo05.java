/**
 * 初始化块：Java中可以通过初始化块进行数据赋值
 * 在类的声明中，可以包含多个初始化块，当创建类的实例时，就会依次执行这些代码块。如果使用 static 修饰初始化块，就称为静态初始化块。
 *
 * 注意：静态初始化块只在类加载时执行，且只会执行一次，同时静态初始化块只能给静态变量赋值，不能初始化普通的成员变量。
 *
 * 程序运行时静态初始化块最先被执行，然后执行普通初始化块，最后才执行构造方法。由于静态初始化块只在类加载时执行一次，所以当再次创建对象 hello2 时并未执行静态初始化块。
 */
package com.paul.hello;

public class Demo05 {

    String name; // 声明变量name
    String sex; // 声明变量sex
    static int age;// 声明静态变量age

    // 构造方法
    public Demo05() {
        System.out.println("通过构造方法初始化name");
        name = "tom";
    }

    // 初始化块
    {
        System.out.println("通过初始化块初始化sex");
        sex = "男";
    }

    // 静态初始化块
    static {
        System.out.println("通过静态初始化块初始化age");
        age = 20;
    }

    public void show() {
        System.out.println("姓名：" + name + "，性别：" + sex + "，年龄：" + age);
    }

    public static void main(String[] args) {
        // 创建对象
        Demo05 hello = new Demo05();
        // 调用对象的show方法
        hello.show();

    }
}