package com.paul.hello;

public class Demo16 {
    public static void main(String[] args) {
        Mobile nokia = new Nokia();
        nokia.call();
        nokia.message();

        Mobile apple = new Apple();
        apple.call();
        apple.message();
    }
}
