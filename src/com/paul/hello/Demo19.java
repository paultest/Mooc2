package com.paul.hello;

import java.util.Scanner;

public class Demo19 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("***************欢迎使用滴滴租车系统***************");
        System.out.println();
        System.out.println("*******租车请按 （1）     退出请按（0）*******");
        int num = sc.nextInt();
        Car[] cars = {new AutoCar("奥迪A4", 300, 4),
                new AutoCar("雪佛兰5", 250, 4),
                new AutoCar("宝骏巴士", 800, 45),
                new Truck("东风Rl", 1000, 10),
                new Truck("依维柯S", 1500, 25),
                new PickUpTruck("皮卡雪Z", 600, 2, 5)};
        if (num != 1) {
            System.out.println("****欢迎下次光临****");
            System.exit(0);
        } else {
            for (int i = 1; i <= cars.length; i++) {
                System.out.println("序号" + i + "——————————-" + cars[i - 1]);
            }
        }
        System.out.println("请输入你要预定的数量");
        int a = sc.nextInt();
        int priceSum = 0;//价格总量
        double goodsLoadSum = 0;//载货总量
        int passengerLoadSum = 0;//载客总量
        for (int i = 1; i <= a; i++) {
            System.out.println("请输入第" + i + "辆要预定车辆的序号");
            int on = sc.nextInt();
            passengerLoadSum = passengerLoadSum + cars[i - 1].getPassengerLoad();
            goodsLoadSum = goodsLoadSum + cars[i - 1].getGoodsLoad();
            priceSum = priceSum + cars[i - 1].getPrice();
        }
        System.out.println("一共需要" + priceSum + "元/天" + "     " + "共载货量：" + goodsLoadSum + "吨     " + "共载客量：" + passengerLoadSum + "人");
        System.out.println("************************谢谢惠顾！");

    }

}