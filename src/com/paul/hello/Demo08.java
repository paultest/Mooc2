/**
 * 如果外部类和内部类具有相同的成员变量或方法，内部类默认访问自己的成员变量或方法，如果要访问外部类的成员变量，可以使用 this 关键字。
 */
package com.paul.hello;

public class Demo08 {
    // 外部类中的成员属性b
    int b = 1;

    public class Inner {
        // 内部类中的成员属性b
        int b = 2;

        public void test() {
            System.out.println("访问外部类中的b: " + Demo08.this.b);
            System.out.println("访问内部类中的b: " + b);
        }
    }

    public static void main(String[] args) {
        Demo08 outer = new Demo08();
        Inner demo = outer.new Inner();
        demo.test();
    }
}
