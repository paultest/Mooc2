package com.paul.hello;

public class Apple extends Mobile{
    @Override
    public void call() {
        System.out.println("苹果手机的打电话功能");
    }

    @Override
    public void message() {
        System.out.println("苹果手机的发短信功能");
    }
}
