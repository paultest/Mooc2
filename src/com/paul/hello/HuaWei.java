package com.paul.hello;

public class HuaWei extends Mobile implements IPlayGame {

    @Override
    public void playGame() {
        System.out.println("华为手机的玩游戏");
    }

    @Override
    public void call() {
        System.out.println("华为手机的打电话");
    }

    @Override
    public void message() {
        System.out.println("华为手机的发短信");
    }
}
