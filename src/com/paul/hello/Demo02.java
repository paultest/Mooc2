package com.paul.hello;

public class Demo02 {

	public static void main(String[] args) {
		// 调用对象的方法
		Telephone phone = new Telephone();
		phone.sendMessage();

		// 给实例变量赋值
		phone.setScreen(5.9f);
		phone.setCpu(6.0f);
		phone.setMem(2.0f);

		// 调用对象的方法
		phone.sendMessage();
		
		// 调用对象的方法
		Telephone phone2 = new Telephone(1.2f, 3.6f, 2.4f);
		phone2.sendMessage();
	}

}