/**
 * 成员内部类的使用方法：
 * 1. Inner 类定义在 Demo07 类的内部，相当于 Demo07 类的一个成员变量的位置，Inner 类可以使用任意访问控制符，如 public 、 protected 、 private 等
 * 2. Inner 类中定义的 test() 方法可以直接访问 Outer 类中的数据，而不受访问控制符的影响，如直接访问 Demo07 类中的私有属性a
 * 3. 定义了成员内部类后，必须使用外部类对象来创建内部类对象，而不能直接去 new 一个内部类对象，即：内部类 对象名 = 外部类对象.new 内部类( );
 *
 * 注意：
 * 1. 外部类是不能直接使用内部类的成员和方法，比如main方法中不能直接调用内部类Inner的test方法
 * 2. 如果外部类和内部类具有相同的成员变量或方法，内部类默认访问自己的成员变量或方法，如果要访问外部类的成员变量，可以使用 this 关键字。
 */

package com.paul.hello;

/**
 * 外部类Demo07
 */
public class Demo07 {
    // 外部类的私有属性
    private int a = 99;

    /**
     * 内部类Inner
     */
    public class Inner {
        // 内部类的成员属性
        int b = 2;

        public void test() {
            System.out.println("访问外部类中的a: " + a);
            System.out.println("访问内部类中的b: " + b);
        }
    }

    /**
     * 测试成员内部类
     * @param args
     */
    public static void main(String[] args) {
        // 创建外部类对象，对象名为outer
        Demo07 outer = new Demo07();

        // 使用外部类对象创建内部类对象，对象名为demo
        Inner demo = outer.new Inner();

        // 调用内部类对象的test方法
        demo.test();
    }
}
