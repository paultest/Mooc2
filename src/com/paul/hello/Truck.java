package com.paul.hello;

public class Truck extends Car {
    public Truck(String name, int price, double goodsLoad) {
        this.setName(name);
        this.setPrice(price);
        this.setGoodsLoad(goodsLoad);
    }

    @Override
    public String toString() {
        return this.getName()+"----"+this.getPrice()+"元/天---- 载货"+this.getGoodsLoad()+"吨";
    }
}
