package com.paul.hello;

public class Telephone {
    private float screen;
    private float cpu;
    private float mem;

    public float getScreen() {
        return screen;
    }

    public void setScreen(float newScreen) {
        this.screen = newScreen;
    }

    public float getCpu() {
        return cpu;
    }

    public void setCpu(float newCpu) {
        this.cpu = newCpu;
    }

    public float getMem() {
        return mem;
    }

    public void setMem(float newMem) {
        this.mem = newMem;
    }

    /**
     * 打电话功能
     */
    void call() {
        System.out.println("Telephone有打电话的功能");
    }

    /**
     * 发送短信功能
     */
    void sendMessage() {
        System.out.println("screen:" + screen + " cpu:" + cpu + " mem:" + mem + " Telephone有发短信的功能");
    }

    /**
     * 无参数的构造方法
     */
    public Telephone() {
        System.out.println("这个是无参的构造方法");
    }

    /**
     * 有参数的构造方法
     *
     * @param newScreen
     * @param newCpu
     * @param newMem
     */
    public Telephone(float newScreen, float newCpu, float newMem) {
        if (newScreen < 3.5) {
            System.out.println("您输入的屏幕参数有问题，自动为您赋值3.5");
            screen = 3.5f;
        } else {
            screen = newScreen;
        }

        cpu = newCpu;
        mem = newMem;
    }
}