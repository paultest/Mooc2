package com.paul.hello;

public abstract class Mobile {
    public abstract void call();

    public abstract void message();
}
