package com.paul.hello;

import java.util.Objects;

public class Dog extends Animal {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dog)) return false;
        Dog dog = (Dog) o;
        return age == dog.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(age);
    }

    @Override
    public String toString() {
        return "Dog{" +
                "age=" + age +
                '}';
    }

    public int age = 20;

    public void eat() {
        System.out.println("狗具有吃的能力");
    }

    public Dog() {
        age = 30;
        System.out.println("Dog类执行了");
        System.out.println("Dog类的age是：" + age);
    }

    public void method() {
        System.out.println("父类Animal的age为：" + super.age);
        System.out.println("子类Dog的age为：" + age);
        System.out.println("调用父类Animal的eat方法：");
        super.eat();
    }
}
