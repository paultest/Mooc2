package com.paul.hello;

public class Animal {
    public int age = 10;

    public String name;

    public void eat() {
        System.out.println("动物具有吃的能力");
    }

    public Animal() {
        System.out.println("动物类执行了");
    }
}
