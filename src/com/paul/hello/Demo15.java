package com.paul.hello;

public class Demo15 {
    public static void main(String[] args) {
        Dog dog = new Dog();

        // 向上类型转换，自动类型提升
        Animal animal = dog;

        // 向下类型转换，强制类型转换
        Dog dog2 = (Dog)animal;

        // 向下类型转换，强制类型转换，有风险，是会报错的，编译时强制把Animal类型转换为Cat类型，但是由于Animal类型是由Dog类型向上转换而来的，即Dog不能转换为Cat类型
        // Cat cat = (Cat)animal;

        // 判断是否可以进行类型转换
        if (animal instanceof Cat) {
            Cat cat = (Cat)animal;
        } else {
            System.out.println("无法进行类型转换");
        }
    }
}
