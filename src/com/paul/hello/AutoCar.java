package com.paul.hello;

public class AutoCar extends Car {
    public AutoCar(String name, int price, int passengerLoad) {
        this.setName(name);
        this.setPrice(price);
        this.setPassengerLoad(passengerLoad);
    }

    @Override
    public String toString() {
        return this.getName() + "----" + this.getPrice() + "元/天---- 载客" + this.getPassengerLoad() + "人";
    }
}
