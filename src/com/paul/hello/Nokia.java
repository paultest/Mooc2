package com.paul.hello;

public class Nokia extends Mobile{
    @Override
    public void call() {
        System.out.println("诺基亚手机的打电话");
    }

    @Override
    public void message() {
        System.out.println("诺基亚手机的发短信");
    }
}
